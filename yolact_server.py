import websockets
import asyncio
import numpy as np
import cv2
import orjson
from Yolact import Yolact
import time

if __name__ == '__main__':
    print("Creating detectron2...")
    yolact = Yolact()
    print("Detectron2 created")

    async def detect(websocket, path):  
        image = await websocket.recv()
        print("Starting inference...")
        process_start = time.time()
        pixel_list = np.frombuffer(image, dtype='uint8')
        img = cv2.imdecode(pixel_list, cv2.IMREAD_UNCHANGED)
        print("Image received")
        process_end = time.time()
        print("Server side image processing: ", str(process_end-process_start), "seconds")

        detect_start = time.time()
        result = yolact.detect_image(img)
        detect_end = time.time()
        print("Server side detection time: ", str(detect_end-detect_start), "seconds")

        ser_start = time.time()
        result_json = orjson.dumps(result)
        ser_end = time.time()
        print("Server side serialization: ", str(ser_end-ser_start), "seconds")

        send_start = time.time()
        await websocket.send(result_json)
        send_end = time.time()
        print("Server side sending: ", str(send_end - send_start), "seconds")
        print("Total time: ", str(send_end - process_start))

    print("Starting server...")
    start_server = websockets.serve(detect, port=8767, max_size=None)
    print("Server successfully started!")
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()
